export default class MovieService {
    constructor() {
        this.urlBase = 'http://localhost'
        this.port = '8080'
    }


    sendRequest = async (path, params, options) => {
        const url = buildUrl.call(this, path, params)

        const res = await fetch(url, options)

        if (!res.ok) {
            throw new Error('Something goes wrong')
        }

        return await res.json()
    }

    getFullList = async (params) => {
        return await this.sendRequest('movies', params)
    }

    addItem = async (data) => {
        const headers = new Headers()
        headers.append('Content-Type', 'application/json')

        return await this.sendRequest('movies', {}, {
            headers,
            method: 'post',
            body: JSON.stringify(data)})
    }

    removeItem = async (id) => {
        return await this.sendRequest(`movies/${id}`, {}, {method: 'delete'})
    }

    sendFile = async (fileData) => {

        return await this.sendRequest(`import`, {}, {
            method: 'post',
            body:fileData,
        })
    }

    searchItem = async (search = '', searchBy = 'title') => {
        return await this.sendRequest('movies', {search, searchBy}, {method: 'get'})
    }

    filterItems = async (filterBy) => {
        return await this.sendRequest('movies', {filterBy}, {method: 'get'})
    }
}

const objectToParams = (obj) => {

    const newObj = {...obj}

    for (let k in newObj){
        if(newObj[k] === null){
            delete newObj[k]
        }
    }

    const keys = Object.keys(newObj)

    if(keys.length === 0)
        return ''

    const params = keys.map(el => `${el}=${newObj[el]}`)

    return params.reduce((total, el) => `${total}&${el}`)
}

const buildUrl = function (path, params = {}) {
    path = path ? `/${path}` : ''

    const parameters = objectToParams(params) !== '' ? `?${objectToParams(params)}` : ''

    return `${this.urlBase}:${this.port}${path}${parameters}`
}
