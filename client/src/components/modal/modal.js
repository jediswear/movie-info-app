import React, {Component} from 'react'
import {connect} from 'react-redux'
import {toggleModalAddItem, getMovies, setTotalPages} from '../../actions'
import {withMovieService} from '../hoc'


class Modal extends Component {

    state = {
        title: '',
        releaseYear: '',
        format: '',
        stars: '',
        errors: {
            title: '',
            releaseYear: '',
            format: '',
            stars: '',
        }
    }

    onClose(e){
        const { toggleModalAddItem } = this.props

        if(e.target.classList.contains('overlay') || e.target.classList.contains('close')){
            toggleModalAddItem(false)
            this.setState({
                title: '',
                releaseYear: '',
                format: '',
                stars: '',
                errors: {
                    title: '',
                    releaseYear: '',
                    format: '',
                    stars: '',
                }
            })
        }
    }

    onInput(e){
        const name = e.target.getAttribute('name')
        let val = e.target.value

        this.setState({
            [name]: val
        })
        this.validation({[name]: val})
    }

    onlyNumbers(e){
        const charCode = e.which ? e.which : e.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            e.preventDefault()
    }

    onAddMovie(){
        const { movieService, toggleModalAddItem } = this.props
        const { title, releaseYear, format, stars} = this.state

        const data = {
            title,
            releaseYear,
            format,
            stars
        }

        const isValid = noError(this.validation(data))

        if(isValid){

            movieService.addItem(data)
                .then(() => {
                    this.updateList()
                    toggleModalAddItem(false)
                })
                .catch(err => {
                    console.log(err)
                })
        }
    }

    validation (data){
        const keys = Object.keys(data)
        const newErrors = {...this.state.errors}

        keys.forEach(key => {
            let value = data[key]
            let msg = ''

            switch (key) {
                case 'title':
                    msg = value.length > 0 ? msg : 'Заполните поле'
                    break
                case 'releaseYear':
                    msg = value > 2030 ? 'Дата должна быть меньше 2030' : msg
                    msg = value < 1850 ? 'Дата не должна быть меньше 1850' : msg
                    msg = value.length !== 4 ? 'Введите дату в формате YYYY' : msg
                    break
                case 'format':
                    msg = value.length > 0 ? msg : 'Заполните поле'
                    break
                case 'stars':
                    value = value.split(',')
                    msg = value.toString().length > 0 ? msg : 'Заполните поле'
                    break
            }

            newErrors[key] = msg
        })

        this.setState((state) => {
            return {
                ...state,
                errors: newErrors
            }
        })

        return newErrors
    }

    updateList() {
        const {movieService, getMovies, setTotalPages, paramsForQuery} = this.props

        movieService.getFullList(paramsForQuery)
            .then(({page, totalPages}) => {
                getMovies(page)
                setTotalPages(totalPages)
            })
    }

    render() {

        const { modalAddItem } = this.props
        const { errors } = this.state

        // console.log(modalAddItem)

        if(!modalAddItem)
            return null

        return (
            <div className="overlay" onClick={(e) => this.onClose(e)}>
                <div className="modalContainer">
                    <div className="modalHeader">
                        <h5>Добавить фильм</h5>
                        <button type="button" className="close" onClick={(e) => this.onClose(e)}>×</button>
                    </div>
                    <div className="modalBody">
                        <div className="form-group">
                            <label htmlFor="addModalTitle">Название фильма</label>
                            <input type="text" className={`form-control ${ errors.title ? 'is-invalid' : null}`} id="addModalTitle" name="title" onBlur={(e) => this.onInput(e)}/>
                            { errors.title ? <div className="invalid-feedback">{ errors.title }</div> : null }
                        </div>
                        <div className="form-group">
                            <label htmlFor="addModalYear">Год выпуска</label>
                            <input type="text"
                                   className={`form-control ${ errors.releaseYear ? 'is-invalid' : null}`}
                                   id="addModalYear" name="releaseYear"
                                   onKeyPress={(e) => this.onlyNumbers(e)}
                                   onBlur={(e) => this.onInput(e)}/>
                            { errors.releaseYear ? <div className="invalid-feedback">{ errors.releaseYear }</div> : null }
                        </div>
                        <div className="form-group">
                            <label htmlFor="addModalFormat">Формат носителя</label>
                            <input type="text" className={`form-control ${ errors.format ? 'is-invalid' : null}`} id="addModalFormat" name="format" onBlur={(e) => this.onInput(e)}/>
                            <small id="emailHelp" className="form-text text-muted">Например VHS, DVD, Blu-Ray...</small>
                            { errors.format ? <div className="invalid-feedback">{ errors.format }</div> : null }
                        </div>
                        <div className="form-group">
                            <label htmlFor="addModalStars">Список актеров</label>
                            <input type="text" className={`form-control ${ errors.stars ? 'is-invalid' : null}`} id="addModalStars" name="stars" onBlur={(e) => this.onInput(e)}/>
                            <small id="emailHelp" className="form-text text-muted">Введите имена актеров разделяя их
                                символом ","
                            </small>
                            { errors.stars ? <div className="invalid-feedback">{ errors.stars }</div> : null }
                        </div>
                    </div>
                    <div className="modalFooter">
                        <button className="btn btn-primary" onClick={() => this.onAddMovie()}>Добавить</button>
                    </div>
                </div>
            </div>
        )
    }
}

const noError = errors => {
    let valid = true

    Object.values(errors).forEach(error => {
        if (error !== '')
            valid = false
    })

    return valid
}

const mapStateToProps = ({modalAddItem, paramsForQuery}) => {
    return {
        modalAddItem,
        paramsForQuery
    }
}

const mapDispatchToProps = {
    toggleModalAddItem,
    getMovies,
    setTotalPages
}

export default withMovieService()(connect(mapStateToProps, mapDispatchToProps)(Modal))