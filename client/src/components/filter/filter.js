import React, {Component} from 'react'
import {withMovieService} from '../hoc'
import {getMovies, setTotalPages, setFilterBy} from '../../actions'
import {connect} from 'react-redux'
class Filter extends Component {

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.paramsForQuery.filterBy !== this.props.paramsForQuery.filterBy){
            this.updateList()
        }
    }

    updateList() {
        const {movieService, getMovies, setTotalPages, paramsForQuery} = this.props

        movieService.getFullList(paramsForQuery)
            .then(({page, totalPages}) => {
                getMovies(page)
                setTotalPages(totalPages)
            })
    }

    onChange (e) {
        const val = e.target.value
        const {setFilterBy} = this.props
        let filter

        switch (val) {
            case '1':
                filter = 'title'
                break
            case '2':
                filter = 'releaseYear'
                break
            default:
                break
        }

        setFilterBy(filter)
    }

    render(){
        return(
            <div className="filters">
                <h3>Список фильмов:</h3>
                <div>
                    <div className="form-group">
                        <small id="emailHelp" className="form-text text-muted">Сортировка:</small>
                        <select defaultValue="0" className="form-control" id="exampleSelect2" onChange={(e) => this.onChange(e)}>
                            <option value="0" disabled>Выберите значение</option>
                            <option value="1">По алфавиту</option>
                            <option value="2">По дате</option>
                        </select>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({paramsForQuery}) => {
    return {
        paramsForQuery
    }
}

const mapDispatchToProps = {
    getMovies,
    setTotalPages,
    setFilterBy
}

export default withMovieService()(connect(mapStateToProps,mapDispatchToProps)(Filter))