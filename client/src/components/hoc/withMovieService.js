import React from 'react'
import { MovieServiceConsumer } from '../movie-service-context/movie-service-context'

const withMovieService = () => (Wrapped) =>{
    return (props) => {
        return(
            <MovieServiceConsumer>
                {
                    (api) => {
                        return (
                            <Wrapped {...props} movieService={api} />
                        )
                    }
                }
            </MovieServiceConsumer>
        )
    }
}

export default withMovieService