import React, {Component} from 'react'
import {withMovieService} from '../hoc'
import {connect} from 'react-redux'
import {getMovies, setTotalPages, setSearchQuery, setSearchBy, setCurrentPage, toggleModalAddItem} from '../../actions'
import './header.scss'
import Modal from '../modal'

class Header extends Component {
    onInput(e) {

        const query = e.target.value

        const {setSearchQuery} = this.props

        setSearchQuery(query)

        if(e.keyCode === 13)
            this.onSearchClick()
    }

    onChangeSearchBy(e) {
        const value = e.target.value
        const {setSearchBy} = this.props

        let field

        switch (value) {
            case '1':
                field = 'title'
                break
            case '2':
                field = 'stars'
                break
            default:
                break
        }

        setSearchBy(field)
    }

    onSearchClick() {
        const {setCurrentPage} = this.props
        setCurrentPage(1)

        this.updateList()
    }

    updateList() {
        const {movieService, getMovies, setTotalPages, paramsForQuery, setCurrentPage} = this.props


        movieService.getFullList(paramsForQuery)
            .then(({page, totalPages}) => {
                getMovies(page)
                setTotalPages(totalPages)
            })
            .catch(err => {
                console.log(err)
            })
    }

    onAddMovie() {
        const {toggleModalAddItem} = this.props

        toggleModalAddItem(true)
    }

    onImport = (e) => {
        const file = e.target.files[0]

        const {movieService} = this.props

        const data = new FormData()
        data.append('file', file)


        movieService.sendFile(data)
            .then(() => {
                this.updateList()
            })
            .catch(err => {
                console.log(err)
            })
    }

    render() {

        const {paramsForQuery} = this.props


        return (
            <React.Fragment>
                <Modal/>
                <div className="header">
                    <div className="container">
                        <div className="searchContainer">
                            <div className="form-group searchGroup">
                                <input type="text"
                                       className="form-control"
                                       id="mainSearch"
                                       placeholder="Поиск..."
                                       value={paramsForQuery.search ? paramsForQuery.search : ''}
                                       onChange={(e) => this.onInput(e)}
                                       onKeyDown={(e) => this.onInput(e)}
                                />
                                <button className="searchBtn" onClick={() => this.onSearchClick()}>
                                    <i className="material-icons">search</i>
                                </button>
                            </div>
                            <div className="form-group">
                                <select defaultValue="1" className="form-control" id="exampleSelect2"
                                        onChange={(e) => this.onChangeSearchBy(e)}>
                                    <option value="1">По названию</option>
                                    <option value="2">По актеру</option>
                                </select>
                            </div>
                        </div>
                        <div className="btnsContainer">
                            <div className="addMovie">
                                <button className="btn" onClick={() => this.onAddMovie()}>
                                    <i className="material-icons">add</i>
                                    Добавить
                                </button>
                            </div>
                            <div className="importContainer">

                                <label htmlFor="import">
                                    <i className="material-icons">publish</i>
                                    Импорт
                                </label>
                                <input id="import" className="import" type="file"
                                       onChange={(e) => this.onImport(e)}/>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}


const mapStateToProps = ({currentPage, paramsForQuery}) => {
    return {
        currentPage,
        paramsForQuery,
    }
}

const mapDispatchToProps = {
    getMovies,
    setTotalPages,
    setSearchQuery,
    setSearchBy,
    setCurrentPage,
    toggleModalAddItem
}

export default withMovieService()(connect(mapStateToProps, mapDispatchToProps)(Header))