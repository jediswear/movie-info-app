import React, { Component } from 'react'
import './pagination.scss'
import { connect } from "react-redux";
import { setCurrentPage } from "../../actions";

class Pagination extends Component {
    render() {

        let { viewRange = 4, paramsForQuery:{currentPage}, setCurrentPage, totalPages } = this.props
        const viewArea = []

        if (totalPages === 1) {
            return null
        }

        const rangeStart = currentPage - viewRange >= 0 ? currentPage - Math.floor(viewRange / 2) : 1
        let rangeEnd = currentPage - viewRange >= 0 ? currentPage + Math.floor(viewRange / 2) : viewRange

        /**
         * проверка на максимум старниц
         * */
        rangeEnd = +(rangeEnd <= totalPages ? rangeEnd : totalPages) + 1

        for (let i = rangeStart; i < rangeEnd; i++) {
            const isSelected = currentPage === i ? 'current-page' : null
            const res = <div className={ isSelected }  onClick={ () => setCurrentPage(i) } key={i}>{i}</div>

            viewArea.push(res)
        }

        return (
            <div className="pagination">
                {currentPage > 1 ? <div className="prev-page" onClick={ () => setCurrentPage(currentPage - 1)}>
                    <i className="material-icons">
                        arrow_right_alt
                    </i>
                    {'Назад'}
                </div> : null}
                { viewArea }
                {currentPage < +totalPages ? <div className="next-page" onClick={ () => setCurrentPage(currentPage + 1)}>
                    {'Вперед'}
                    <i className="material-icons">
                        arrow_right_alt
                    </i>
                </div> : null}
            </div>
        )
    }
}

const mapStateToProps = (state) => {

    const { paramsForQuery, totalPages } = state

    return {
        paramsForQuery,
        totalPages
    }
}

const mapDispatchToProps = {
    setCurrentPage
}

export default connect(mapStateToProps, mapDispatchToProps)(Pagination)