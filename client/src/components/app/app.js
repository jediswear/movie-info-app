import React from 'react'
import Header from '../header'
import Filter from '../filter'
import ItemList from '../item-list'
import './app.scss'


const App = () => {
    return (
        <React.Fragment>
            <Header/>
            <div className="mainBody container">
                <Filter/>
                <ItemList/>
            </div>
        </React.Fragment>
    )
}

export default App