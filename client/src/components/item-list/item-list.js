import React, {Component} from 'react'
import {withMovieService} from '../hoc'
import {connect} from 'react-redux'
import {getMovies, setTotalPages, setCurrentPage} from '../../actions'
import Pagination from '../pagination'
import './item-list.scss'

class ItemList extends Component {

    state = {
        showModal: false,
        modalContent: {
            id: null,
            title: null,
            releaseYear: null,
            format: null,
            stars: []
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.paramsForQuery.currentPage !== this.props.paramsForQuery.currentPage){
            this.updateList()
        }
    }

    componentDidMount() {
        this.updateList()
    }

    updateList() {
        const {movieService, getMovies, setTotalPages, paramsForQuery} = this.props

        movieService.getFullList(paramsForQuery)
            .then(({page, totalPages}) => {
                getMovies(page)
                setTotalPages(totalPages)
            })
    }

    onDelete(id) {
        const {movieService} = this.props

        movieService.removeItem(id)
            .then(() => this.updateList())
            .catch(err => {
                console.log(err)
            })
    }

    setMovieInfoForModal = (movieItem) => {
        this.setState({
            showModal: true,
            modalContent: movieItem
        })
    }

    onClose(e){

        if(e.target.classList.contains('overlay') || e.target.classList.contains('close'))
            this.toggleModalMovie(false)
    }

    toggleModalMovie(status){
        this.setState(() => {
            return {
                showModal: status
            }
        })
    }

    render() {

        const { moviesList, paramsForQuery: {currentPage}, setCurrentPage } = this.props

        const { showModal, modalContent } = this.state

        if(currentPage > 1 && moviesList.length === 0){
            setCurrentPage(currentPage -1)
        }

        if (moviesList.length === 0)
            return (
                <div>Здесь пусто...</div>
            )

        const list = moviesList.map((item, i) => {

            const movieIndex = (currentPage - 1) * 10 + i + 1

            return (
                <li key={ i }>
                    <Item title={item.title}
                          index={movieIndex}
                          id={ item['_id'] }
                          releaseYear={item.releaseYear}
                          format={item.format}
                          stars={item.stars}
                          onDelete={(id) => this.onDelete(id)}
                          setMovieInfoForModal={this.setMovieInfoForModal}/>
                </li>
            )
        })

        return (
            <React.Fragment>
                { showModal ?
                    <div className="overlay" onClick={(e) => this.onClose(e)}>
                        <div className="modalContainer">
                            <div className="modalHeader">
                                <h5>Информация о фильме</h5>
                                <button type="button" className="close" onClick={(e) => this.onClose(e)}>×</button>
                            </div>
                            <div className="modalBody">
                                <p><span>ID:</span> {modalContent.id}</p>
                                <p><span>Название:</span> {modalContent.title}</p>
                                <p><span>Год выпуска:</span> {modalContent.releaseYear}</p>
                                <p><span>Формат:</span> {modalContent.format}</p>
                                <p><span>Список актеров:</span> {modalContent.stars.join(', ')}</p>
                            </div>
                        </div>
                    </div>
                    : null
                }
                <ul className="moviesList">
                    {list}
                </ul>
                <Pagination />
            </React.Fragment>
        )
    }
}

const Item = ({title = 'No title', releaseYear, id, index, onDelete, setMovieInfoForModal, format, stars}) => {
    return (
        <React.Fragment>
            <span className="title" onClick={() => setMovieInfoForModal({
                id,
                title,
                releaseYear,
                format,
                stars
            })}>{index}. {title} ({releaseYear})</span>
            <span className="removeItem" onClick={() => onDelete(id)}>
                <i className="material-icons">delete_forever</i>
            </span>
        </React.Fragment>
    )
}

const mapStateToProps = ({moviesList, totalPages, paramsForQuery}) => ({
    moviesList,
    totalPages,
    paramsForQuery
})

const mapDispatchToProps = {
    getMovies,
    setTotalPages,
    setCurrentPage
}

export default withMovieService()(
    connect(mapStateToProps, mapDispatchToProps)(ItemList)
)