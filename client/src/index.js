import React from 'react'
import ReactDOM from 'react-dom'
import {MovieService} from './services'
import {MovieServiceProvider} from './components/movie-service-context'
import store from './store'
import {Provider} from 'react-redux'

import App from './components/app'


const movieApi = new MovieService()
const root = document.getElementById('root')

ReactDOM.render(
    <Provider store={store}>
        <MovieServiceProvider value={movieApi}>
            <App/>
        </MovieServiceProvider>
    </Provider>,
    root)
