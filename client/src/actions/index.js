const getMovies = (moviesList) => {
    return {
        type: 'FETCH_MOVIES_SUCCESS',
        payload: moviesList
    }
}

const setCurrentPage = (pageIndex) => {
    return {
        type: 'SET_CURRENT_PAGE',
        payload: pageIndex
    }
}

const setTotalPages = (totalIndex) => {
    return {
        type: 'SET_TOTAL_PAGES',
        payload: totalIndex
    }
}

const setSearchQuery = (query) => {
    return {
        type: 'SET_SEARCH_QUERY',
        payload: query
    }
}

const setSearchBy = (searchBy) => {
    return {
        type: 'SET_SEARCH_BY',
        payload: searchBy
    }
}

const setFilterBy = (filterBy) => {
    return {
        type: 'SET_FILTER_BY',
        payload: filterBy
    }
}

const toggleModalAddItem = (isVisible) => {
    return {
        type: 'TOGGLE_MODAL_ADD_ITEM',
        payload: isVisible
    }
}

export {
    getMovies,
    setCurrentPage,
    setTotalPages,
    setSearchQuery,
    setSearchBy,
    setFilterBy,
    toggleModalAddItem
}