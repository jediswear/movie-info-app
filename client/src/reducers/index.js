const initialState = {
    moviesList: [],
    totalPages: 1,
    paramsForQuery: {
        search: '',
        searchBy: 'title',
        filterBy: null,
        currentPage: 1,
    },
    modalAddItem: false
}

const reducer = (state = initialState, action) => {


    switch (action.type) {
        case 'FETCH_MOVIES_SUCCESS':
            return {
                ...state,
                moviesList: action.payload
            }
        case 'SET_CURRENT_PAGE':
            return {
                ...state,
                paramsForQuery: {
                    ...state.paramsForQuery,
                    currentPage: action.payload
                }
            }
        case 'SET_TOTAL_PAGES':
            return {
                ...state,
                totalPages: action.payload
            }
        case 'SET_SEARCH_QUERY':
            return {
                ...state,
                paramsForQuery: {
                    ...state.paramsForQuery,
                    search: action.payload
                }
            }
        case 'SET_SEARCH_BY':
            return {
                ...state,
                paramsForQuery: {
                    ...state.paramsForQuery,
                    searchBy: action.payload
                }
            }
        case 'SET_FILTER_BY':
            return {
                ...state,
                paramsForQuery: {
                    ...state.paramsForQuery,
                    filterBy: action.payload
                }
            }
        case 'TOGGLE_MODAL_ADD_ITEM':
            return {
                ...state,
                modalAddItem: action.payload
            }
        default:
            return state
    }
}

export default reducer