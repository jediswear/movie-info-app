import fs from 'fs'
import MoviesData from './modules/Data'
import express from 'express'
import fileUpload from 'express-fileupload'
import * as db from './utils/DataBaseUtils'
import { serverPort } from './config/config'
import bodyParser from 'body-parser'
import cors from 'cors'

db.setUpConnection()

const app = express()
const server = app.listen(serverPort, () => {
    console.log(`server is up and running on port ${serverPort}`);
})

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json())
app.use(cors())
app.use(fileUpload())

app.get('/movies', (req, res) => {

    const elemsPerPage = 10
    const { currentPage, search, searchBy, filterBy } = req.query


    db.listMovies()
        .then((data) => {

            let totalPages

            if(search || searchBy){
                // console.log('search true:', search)
                data = data.filter(movie => {
                    const title = movie[searchBy].toString()

                    if (title.toLowerCase().includes(search.toLowerCase())){
                        return movie
                    }
                })
            }

            if(filterBy){
                // console.log('filter true:', filterBy)
                data = sortBy(data, filterBy)
            }

            if (currentPage){
                // console.log('page true:', currentPage)
                totalPages = Math.ceil(data.length / elemsPerPage)

                data = getPage(data, currentPage, elemsPerPage)
            }

            return res.send({
                totalPages,
                page: data
            })
        })
})

app.post('/movies', (req, res) => {

    db.createMovie(req.body)
        .then(data => res.send(data))
        .catch(err => res.send(err))
})

app.post('/import', (req, res) => {
    const file = req.files.file
    const path = `${__dirname}/uploads/${file.name}`

    file.mv(path, err => {
        if(err){
            console.log(err)
            return
        }

        const textData = fs.readFileSync(path, 'utf-8')
        const moviesData = new MoviesData(textData)
        const importedMovies = moviesData.parse()

        db.listMovies()
            .then(bdMovies => {

                const newMovies = getNonRepeated(bdMovies, importedMovies)

                newMovies.forEach(movieItem => {
                    db.createMovie(movieItem)
                })

                const elemsPerPage = 10
                const data = [...bdMovies,...newMovies]
                const totalPages = Math.ceil(data.length / elemsPerPage)

                return res.send({
                    totalPages,
                    page: getPage(data, 1, elemsPerPage)
                })

            })

    })
})

app.delete('/movies/:id', (req, res) => {
    // console.log('params-id',req.params.id);

    db.deleteMovie(req.params.id)
        .then(data => res.send(data))
})



function sortBy(array, method) {

    switch (method) {
        case 'title':
            return array.sort(strComparator)
            break
        case 'releaseYear':
            return array.sort(dateComparator)
            break
        default:
            console.log('default sort')
            break
    }

}

function dateComparator({releaseYear: a}, {releaseYear: b}) {
    return b - a
}

function strComparator({title: a}, {title:b}) {
    let minLength = Math.min(a.length, b.length);
    let aSub = a.substring(0, minLength);
    let bSub = b.substring(0, minLength);
    return aSub.localeCompare(bSub, undefined, {caseFirst:'upper'});
}

function getNonRepeated(mainArray, compareArray) {
    const nonRepeated = []

    compareArray.forEach(compItem => {

        const isExist = mainArray.find(el => el.title === compItem.title)

        if(!isExist)
            nonRepeated.push(compItem)

    })

    return nonRepeated
}

function getPage(array, page = 1, itemsPerPage){
    page--
    const start = page * itemsPerPage
    const end = start + itemsPerPage < array.length ? start + itemsPerPage : array.length
    const res = []

    for (let i = start; i < end; i++){
        res.push(array[i])
    }

    return res
}
