import mongoose from 'mongoose'

const Schema = mongoose.Schema

const MovieSchema = new Schema({
    title: String,
    releaseYear: Number,
    format: String,
    stars: [String]
})

const Movie = mongoose.model('Movie', MovieSchema)