import mongoose from 'mongoose'
import '../models/Movies'
import {db} from '../config/config'

const Movie = mongoose.model('Movie')

export function setUpConnection() {
    mongoose.connect(`mongodb://${db.host}:${db.port}/${db.name}`, {useNewUrlParser: true})
}

export function listMovies() {
    return Movie.find()
}

export function createMovie(data) {

    const { title, releaseYear, format, stars } = data

    const movie = new Movie({
        title,
        releaseYear,
        format,
        stars
    })
    return movie.save()
}

export function deleteMovie(id) {
    // console.log(id);
    return Movie.findById(id).remove()
}