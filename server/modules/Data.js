class MoviesData {
    constructor(str){
        this.string = str
    }

    parse(){
        const strList = this.string.trim().split('\n\n')
        this.data = getMoviesArray(strList)
        return this.data
    }
}

function getMoviesArray(arrayOfMoviesStr) {
    const newData = []
    arrayOfMoviesStr.forEach(el => {
        newData.push(movieStrToObj(el))
    })

    return newData
}

function movieStrToObj(fields) {
    let obj = {}

    fields.split('\n').forEach(field => {
        obj = {...obj, ...strToObj(field)}
    })

    obj.stars = obj.stars.split(',').map(el => el.trim())

    return obj
}

function strToObj(str) {
    const pare = str.split(':')
    let key = pare[0].replace(' ', '')
    key = key.replace(key[0], key[0].toLowerCase())
    const value = pare[1].trim()
    return {[key] : value}
}

export default MoviesData